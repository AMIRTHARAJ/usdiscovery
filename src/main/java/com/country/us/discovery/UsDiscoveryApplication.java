package com.country.us.discovery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class UsDiscoveryApplication {

	public static void main(String[] args) {
		SpringApplication.run(UsDiscoveryApplication.class, args);
	}

}
